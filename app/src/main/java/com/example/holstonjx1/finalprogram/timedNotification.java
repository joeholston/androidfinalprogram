package com.example.holstonjx1.finalprogram;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.support.v4.app.NotificationCompat;

import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.TimerTask;

/**
 * Created by BELSTERLINGJB1 on 12/18/2017.
 */

public class timedNotification extends TimerTask {
    @Override
    public void run() {
        singleton s = singleton.getInstance();
        Context temp = s.getAContext();
        boolean cur = false;
        DateFormat df1 = DateFormat.getDateInstance();
        try {
            for (int i = 0; i < s.getNearby().size(); i++) {
                if (df1.parse(s.getNearby().get(i).date).compareTo(Calendar.getInstance().getTime()) > 0) {
                //if (df1.parse("December 20, 2017").compareTo(Calendar.getInstance().getTime()) > 0) {
                    cur = true;
                    break;
                }
            }

        }catch (Exception e) {
           e.printStackTrace();
        }
        System.out.println("Checked for current dates");
        if (cur) {
            NotificationManager mNotificationManager = (NotificationManager) temp.getSystemService(Context.NOTIFICATION_SERVICE);
            String channel_ID = "com.example.holstonjx1.finalprogram";
            CharSequence title = "Nearby Concerts";
            String description = "There are concerts nearby.";
            int importance = NotificationManager.IMPORTANCE_HIGH;

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                NotificationChannel channel = new NotificationChannel(channel_ID, title, importance);
                channel.setDescription(description);
                channel.enableLights(true);
                channel.setLightColor(R.style.AppTheme);
                channel.enableVibration(true);
                channel.setVibrationPattern(new long[]{1000});
                mNotificationManager.createNotificationChannel(channel);
            }

            NotificationCompat.Builder builder = new NotificationCompat.Builder(temp, channel_ID)
                    .setSmallIcon(R.drawable.top_logo)
                    .setContentTitle(title)
                    .setContentText(description)
                    .setChannelId(channel_ID);

            PendingIntent contentIntent = PendingIntent.getActivity(temp, 0, new Intent(temp, MainActivity.class), PendingIntent.FLAG_UPDATE_CURRENT);
            /* Intent resultIntent = new Intent(getActivity(), HomeFragment.class);
                TaskStackBuilder stackBuilder = TaskStackBuilder.create(getActivity());
                stackBuilder.addParentStack(getActivity());
                stackBuilder.addNextIntent(resultIntent);
                PendingIntent pendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT); */
            builder.setContentIntent(contentIntent);
            builder.setAutoCancel(true);
            mNotificationManager.notify(1, builder.build());
            SharedPreferences sharedPref = temp.getSharedPreferences("common_file", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPref.edit();
            editor.putBoolean("notification", true);
            editor.commit();
        }
    }
}
