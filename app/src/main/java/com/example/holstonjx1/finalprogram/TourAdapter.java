package com.example.holstonjx1.finalprogram;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;

/**
 * Created by HOLSTONJX1 on 11/15/2017.
 */

public class TourAdapter extends BaseAdapter {
    private ArrayList<ShowInfo> shows;
    private Context context;
    public TourAdapter(Context c, ArrayList<ShowInfo> s){
        context = c;
        shows = s;
    }

    @Override
    public int getCount() {
        return shows.size();
    }

    @Override
    public Object getItem(int i) {
        return shows.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if(view == null){
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.cell_layout_tour, viewGroup, false);
        }

        TextView tourDate = view.findViewById(R.id.date);
        TextView tourCity = view.findViewById(R.id.city);
        TextView tourVenue = view.findViewById(R.id.venue);

        tourDate.setText(shows.get(i).date);
        tourCity.setText(shows.get(i).city);
        tourVenue.setText(shows.get(i).venue);

        return view;
    }


}
