package com.example.holstonjx1.finalprogram;

import android.graphics.BlurMaskFilter;

import java.util.ArrayList;

/**
 * Created by SIMEKQW1 on 12/6/2017.
 */

public class album {
    public String title;
    public ArrayList<String> tracks = new ArrayList<String>();
    public ArrayList<String> videos = new ArrayList<String>();

    album()
    {

    }

    album(String temp)
    {
        if(temp.equals("Blurryface"))
        {
            tracks.add("Heavydirtysoul");
            tracks.add("Stressed Out");
            tracks.add("Ride");
            tracks.add("Fairly Local");
            tracks.add("Tear in My Heart");
            tracks.add("Lane Boy");
            tracks.add("The Judge");
            tracks.add("Doubt");
            tracks.add("Polarize");
            tracks.add("We Don't Believe What's on TV");
            tracks.add("Message Man");
            tracks.add("Hometown");
            tracks.add("Not Today");
            tracks.add("Goner");

            videos.add("https://www.youtube.com/watch?v=r_9Kf0D5BTs&list=PL3roRV3JHZzZd6SeYqYhPFFOm5pBlAQWg");
            videos.add("https://www.youtube.com/watch?v=pXRviuL6vMY&list=PL3roRV3JHZzZd6SeYqYhPFFOm5pBlAQWg&index=2");
            videos.add("https://www.youtube.com/watch?v=Pw-0pbY9JeU&index=3&list=PL3roRV3JHZzZd6SeYqYhPFFOm5pBlAQWg");
            videos.add("https://www.youtube.com/watch?v=HDI9inno86U&list=PL3roRV3JHZzZd6SeYqYhPFFOm5pBlAQWg&index=4");
            videos.add("https://www.youtube.com/watch?v=nky4me4NP70&list=PL3roRV3JHZzZd6SeYqYhPFFOm5pBlAQWg&index=5");
            videos.add("https://www.youtube.com/watch?v=ywvRgGAd2XI&index=6&list=PL3roRV3JHZzZd6SeYqYhPFFOm5pBlAQWg");
            videos.add("https://www.youtube.com/watch?v=PbP-aIe51Ek&list=PL3roRV3JHZzZd6SeYqYhPFFOm5pBlAQWg&index=7");
            videos.add("https://www.youtube.com/watch?v=MEiVnNNpJLA&index=8&list=PL3roRV3JHZzZd6SeYqYhPFFOm5pBlAQWg");
            videos.add("https://www.youtube.com/watch?v=MiPBQJq49xk&index=9&list=PL3roRV3JHZzZd6SeYqYhPFFOm5pBlAQWg");
            videos.add("https://www.youtube.com/watch?v=zZEumf7RowI&list=PL3roRV3JHZzZd6SeYqYhPFFOm5pBlAQWg&index=10");
            videos.add("https://www.youtube.com/watch?v=iE_54CU7Fxk&index=11&list=PL3roRV3JHZzZd6SeYqYhPFFOm5pBlAQWg");
            videos.add("https://www.youtube.com/watch?v=pJtlLzsDICo&list=PL3roRV3JHZzZd6SeYqYhPFFOm5pBlAQWg&index=12");
            videos.add("https://www.youtube.com/watch?v=yqem6k_3pZ8&list=PL3roRV3JHZzZd6SeYqYhPFFOm5pBlAQWg&index=13");
            videos.add("https://www.youtube.com/watch?v=3J5mE-J1WLk&list=PL3roRV3JHZzZd6SeYqYhPFFOm5pBlAQWg&index=14");
        }

        if(temp.equals("Vessel"))
        {
            tracks.add("Ode to Sleep");
            tracks.add("Holding on to You");
            tracks.add("Migraine");
            tracks.add("House of Gold");
            tracks.add("Car Radio");
            tracks.add("Semi-Automatic");
            tracks.add("Screen");
            tracks.add("The Run and Go");
            tracks.add("Fake You Out");
            tracks.add("Guns for Hands");
            tracks.add("Trees");
            tracks.add("Truce");

            videos.add("https://www.youtube.com/watch?v=2OnO3UXFZdE&list=PLEVeIFpNIgYP2USyy-8HRv_QEt99H_ijS");
            videos.add("https://www.youtube.com/watch?v=ktBMxkLUIwY&index=2&list=PLEVeIFpNIgYP2USyy-8HRv_QEt99H_ijS");
            videos.add("https://www.youtube.com/watch?v=Bs92ejAGLdw&index=3&list=PLEVeIFpNIgYP2USyy-8HRv_QEt99H_ijS");
            videos.add("https://www.youtube.com/watch?v=mDyxykpYeu8&list=PLEVeIFpNIgYP2USyy-8HRv_QEt99H_ijS&index=4");
            videos.add("https://www.youtube.com/watch?v=92XVwY54h5k&list=PLEVeIFpNIgYP2USyy-8HRv_QEt99H_ijS&index=5");
            videos.add("https://www.youtube.com/watch?v=pGb6KYJ3qpA&index=6&list=PLEVeIFpNIgYP2USyy-8HRv_QEt99H_ijS");
            videos.add("https://www.youtube.com/watch?v=NK7WWbXlkj4&index=7&list=PLEVeIFpNIgYP2USyy-8HRv_QEt99H_ijS");
            videos.add("https://www.youtube.com/watch?v=wGbraQdkct8&index=8&list=PLEVeIFpNIgYP2USyy-8HRv_QEt99H_ijS");
            videos.add("https://www.youtube.com/watch?v=KnthhE071-I&list=PLEVeIFpNIgYP2USyy-8HRv_QEt99H_ijS&index=9");
            videos.add("https://www.youtube.com/watch?v=Pmv8aQKO6k0&index=10&list=PLEVeIFpNIgYP2USyy-8HRv_QEt99H_ijS");
            videos.add("https://www.youtube.com/watch?v=szp9x1ZlZn4&list=PLEVeIFpNIgYP2USyy-8HRv_QEt99H_ijS&index=11");
            videos.add("https://www.youtube.com/watch?v=eCeBNwBUkcI");
        }

        if(temp.equals("Twenty One Pilots"))
        {
            tracks.add("Implicit Demand for Proof");
            tracks.add("Fall Away");
            tracks.add("The Pantaloon");
            tracks.add("Addict With a Pen");
            tracks.add("Friend, Please");
            tracks.add("March to the Sea");
            tracks.add("Johnny Boy");
            tracks.add("Oh Ms. Believer");
            tracks.add("Air Catcher");
            tracks.add("Trapdoor");
            tracks.add("A Car, a Torch, a Death");
            tracks.add("Taxi Cab");
            tracks.add("Before You Start Your Day");
            tracks.add("Isle of Flightless Birds");

            videos.add("https://www.youtube.com/watch?v=hfrXmKg5xKw&list=PLnMcjPFPHV0sjrK8fE9q9tUdAJkx1aHzb");
            videos.add("https://www.youtube.com/watch?v=ypMACz5ePNg&list=PLnMcjPFPHV0sjrK8fE9q9tUdAJkx1aHzb&index=2");
            videos.add("https://www.youtube.com/watch?v=TBkL09zz24w&list=PLnMcjPFPHV0sjrK8fE9q9tUdAJkx1aHzb&index=3");
            videos.add("https://www.youtube.com/watch?v=0PqpRnpE4yg&index=4&list=PLnMcjPFPHV0sjrK8fE9q9tUdAJkx1aHzb");
            videos.add("https://www.youtube.com/watch?v=4-Q_0iKNnVM&list=PLnMcjPFPHV0sjrK8fE9q9tUdAJkx1aHzb&index=5");
            videos.add("https://www.youtube.com/watch?v=VE6eRwMFx8E&list=PLnMcjPFPHV0sjrK8fE9q9tUdAJkx1aHzb&index=6");
            videos.add("https://www.youtube.com/watch?v=Zbx6yRzx_50&list=PLnMcjPFPHV0sjrK8fE9q9tUdAJkx1aHzb&index=7");
            videos.add("https://www.youtube.com/watch?v=IpiaYAGjXhU&list=PLnMcjPFPHV0sjrK8fE9q9tUdAJkx1aHzb&index=8");
            videos.add("https://www.youtube.com/watch?v=1hPHM6TuQSA&index=9&list=PLnMcjPFPHV0sjrK8fE9q9tUdAJkx1aHzb");
            videos.add("https://www.youtube.com/watch?v=C3aY6wcMJnw&list=PLnMcjPFPHV0sjrK8fE9q9tUdAJkx1aHzb&index=10");
            videos.add("https://www.youtube.com/watch?v=OLKlFw2ab1M&index=11&list=PLnMcjPFPHV0sjrK8fE9q9tUdAJkx1aHzb");
            videos.add("https://www.youtube.com/watch?v=M2wB0skdJ5o&list=PLnMcjPFPHV0sjrK8fE9q9tUdAJkx1aHzb&index=12");
            videos.add("https://www.youtube.com/watch?v=0KIyt6mwmG8&index=13&list=PLnMcjPFPHV0sjrK8fE9q9tUdAJkx1aHzb");
            videos.add("https://www.youtube.com/watch?v=NXEcli7b04Y&list=PLnMcjPFPHV0sjrK8fE9q9tUdAJkx1aHzb&index=14");
        }
    }
}
