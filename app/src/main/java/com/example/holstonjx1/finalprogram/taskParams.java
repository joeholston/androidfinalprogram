package com.example.holstonjx1.finalprogram;

import android.content.Context;
import android.widget.ListView;

import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;

/**
 * Created by SIMEKQW1 on 12/18/2017.
 */

public class taskParams {
    LatLng location;
    ArrayList<ShowInfo> data = new ArrayList<>();
    Context tContext;
    ListView list;

    taskParams(LatLng temp,  ArrayList<ShowInfo> temp2, Context temp3, ListView temp4)
    {
        location = temp;
        data = temp2;
        tContext = temp3;
        list = temp4;
    }
}
