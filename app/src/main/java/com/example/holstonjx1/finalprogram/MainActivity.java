package com.example.holstonjx1.finalprogram;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements TourFragment.OnFragmentInteractionListener, HomeFragment.OnFragmentInteractionListener, MusicFragment.OnFragmentInteractionListener, NearbyFragment.OnFragmentInteractionListener {

    private LocationManager lm;
    private Context mContext;
    public MenuItem frag; // to store the item equal to the Nearby tab (so that on a notification click the navigation bar focus can be updated)
    public BottomNavigationView nav;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            singleton s = singleton.getInstance(mContext);
            switch (item.getItemId()) {
                case R.id.navigation_tour:
                    s.curPage = 1;
                    TourFragment tourFrag = TourFragment.newInstance("", "");
                    FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                    transaction.replace(R.id.container, tourFrag);
                    transaction.commit();
                    return true;
                case R.id.navigation_music:
                    s.curPage = 3;
                    MusicFragment musicFrag = MusicFragment.newInstance("", "");
                    FragmentTransaction transaction2 = getSupportFragmentManager().beginTransaction();
                    transaction2.replace(R.id.container, musicFrag);
                    transaction2.commit();
                    return true;
                case R.id.navigation_nearby_concerts:
                    s.curPage = 2;
                    NearbyFragment nearbyFrag = NearbyFragment.newInstance("", "");
                    FragmentTransaction transaction3 = getSupportFragmentManager().beginTransaction();
                    transaction3.replace(R.id.container, nearbyFrag);
                    transaction3.commit();
                    frag = item; // sets the frag MenuItem equal to item, which equals the Nearby tab in this case --> what is needed
                    return true;
                case R.id.navigation_home:
                    s.curPage = 0;
                    HomeFragment homeFrag = HomeFragment.newInstance("", "");
                    FragmentTransaction transaction4 = getSupportFragmentManager().beginTransaction();
                    transaction4.replace(R.id.container, homeFrag);
                    transaction4.commit();
                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        singleton s = singleton.getInstance(this);
        mContext = this;
        if(s.curPage == -1 || s.curPage == 0) {
            HomeFragment homeFrag = HomeFragment.newInstance("", "");
            FragmentTransaction transaction4 = getSupportFragmentManager().beginTransaction();
            transaction4.replace(R.id.container, homeFrag);
            transaction4.commit();
        }
        if(s.curPage == 1) {
            TourFragment tourFragment = TourFragment.newInstance("", "");
            FragmentTransaction transaction4 = getSupportFragmentManager().beginTransaction();
            transaction4.replace(R.id.container, tourFragment);
            transaction4.commit();
        }
        if(s.curPage == 2) {
            NearbyFragment nearbyFragment = NearbyFragment.newInstance("", "");
            FragmentTransaction transaction4 = getSupportFragmentManager().beginTransaction();
            transaction4.replace(R.id.container, nearbyFragment);
            transaction4.commit();
        }
        if(s.curPage == 3) {
            MusicFragment musicFragment = MusicFragment.newInstance("", "");
            FragmentTransaction transaction4 = getSupportFragmentManager().beginTransaction();
            transaction4.replace(R.id.container, musicFragment);
            transaction4.commit();
        }

        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        nav = navigation;
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if(intent != null)
        {
            NearbyFragment nearbyFrag = NearbyFragment.newInstance("", "");
            FragmentTransaction transaction3 = getSupportFragmentManager().beginTransaction();
            transaction3.replace(R.id.container, nearbyFrag);
            transaction3.commit();

            BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
            nav.setSelectedItemId(frag.getItemId()); // changes notification bar focus to be on the Nearby fragment
            navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        }
    }
}
