package com.example.holstonjx1.finalprogram;

import android.content.Context;
import android.os.AsyncTask;

import com.google.android.gms.maps.model.LatLng;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.content.pm.PackageManager;
import android.widget.ListView;

/**
 * Created by SIMEKQW1 on 12/18/2017.
 */

public class nearbyThread extends AsyncTask<taskParams, Void, ArrayList<ShowInfo>> {
    public taskParams info;
    public ListView nearbyListView;
    public LatLng deviceLocation;
    public ArrayList<ShowInfo> data = new ArrayList<>();
    public Context c;
    public ArrayList<ShowInfo> nearbyData = new ArrayList<ShowInfo>();

    nearbyThread()
    {}

    @Override
    protected ArrayList<ShowInfo> doInBackground(taskParams... taskParams) {
        info = taskParams[0];
        nearbyListView = info.list;
        deviceLocation = info.location;
        data = info.data;
        c = info.tContext;

        for(int i=0; i<data.size(); i++) {
            LatLng concertLocation = getLocationFromAddress(c, data.get(i).city);
            if (concertLocation != null) {
                float results[] = new float[1];
                //returns answer in meters
                android.location.Location.distanceBetween(deviceLocation.latitude, deviceLocation.longitude, concertLocation.latitude, concertLocation.longitude, results);
                int miles = toMiles(results[0]);
                if(miles<100) {
                    nearbyData.add(data.get(i));
                    //System.out.println("Miles: " + miles);
                }
            }
        }

        return nearbyData;
    }

    @Override
    protected void onPostExecute(ArrayList<ShowInfo> showInfos) {
        super.onPostExecute(showInfos);

        TourAdapter nearbyAdapter = new TourAdapter(c, nearbyData);
        singleton s = singleton.getInstance(c);
        s.giveNearby(nearbyData);
        nearbyListView.setAdapter(nearbyAdapter);
        s.nearbyLoaded = true;

    }

    //https://stackoverflow.com/questions/3574644/how-can-i-find-the-latitude-and-longitude-from-address
    private LatLng getLocationFromAddress(Context context, String strAddress) {

        Geocoder coder = new Geocoder(context);
        List<Address> address;
        LatLng position = null;

        try {
            // May throw an IOException
            address = coder.getFromLocationName(strAddress, 5);
            if (address == null || address.size() == 0) {
                return null;
            }
            Address location = address.get(0);
            location.getLatitude();
            location.getLongitude();

            position = new LatLng(location.getLatitude(), location.getLongitude());

        } catch (IOException ex) {

            ex.printStackTrace();
        }
        return position;
    }

    private int toMiles(float m){
        double miles = m*0.000621371;
        int rounded = (int) miles;
        return rounded;
    }
}
