package com.example.holstonjx1.finalprogram;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import com.google.android.gms.maps.model.LatLng;

import java.io.IOException;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link NearbyFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link NearbyFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class NearbyFragment extends Fragment implements singletonDelegate {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private Timer timer = new Timer();
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private boolean loaded = false;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public ListView nearbyListView;
    private ArrayList<ShowInfo> data = new ArrayList<>();
    public ArrayList<ShowInfo> nearbyData;
    private LatLng deviceLocation;
    private LocationManager lm;
    private boolean temp = true;
    public Button demo_Button;

    private OnFragmentInteractionListener mListener;

    public NearbyFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment NearbyFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static NearbyFragment newInstance(String param1, String param2) {
        NearbyFragment fragment = new NearbyFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @SuppressLint("MissingPermission")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_nearby, container, false);

        nearbyListView = v.findViewById(R.id.nearbyListView);
        demo_Button = v.findViewById(R.id.demo_Button);

        singleton s = singleton.getInstance(getContext());
        s.loadData(NearbyFragment.this);

        timedNotification tm1 = new timedNotification();
        Calendar now = Calendar.getInstance();
        GregorianCalendar toTrigger = new GregorianCalendar();
        toTrigger.set(Calendar.HOUR_OF_DAY, 12);
        toTrigger.set(Calendar.MINUTE, 0);
        toTrigger.set(Calendar.SECOND, 0);
        //If the time is already past (or will be in 30s) then we hold off until tomorrow to notify.
        if(toTrigger.getTimeInMillis()<now.getTimeInMillis()+30000) {
            toTrigger.set(Calendar.DAY_OF_MONTH, now.get(Calendar.DAY_OF_MONTH) + 1);
            toTrigger.set(Calendar.HOUR_OF_DAY, 12);
        }

        java.util.Date trigger = toTrigger.getTime();
        timer.schedule(tm1, trigger);

        if(s.nearbyLoaded) {
            nearbyData = s.getNearby();
            TourAdapter nearbyAdapter = new TourAdapter(getContext(), nearbyData);
            nearbyListView.setAdapter(nearbyAdapter);
        }
        //Allows the demo notifications to be triggered.
        nearbyListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                singleton sing = singleton.getInstance(getContext());
                nearbyData = sing.getNearby();
                String venue = nearbyData.get(position).venue;
                String city = nearbyData.get(position).city;
                Intent intent = new Intent(getActivity(), TourMapsActivity.class);
                intent.putExtra("venue", venue);
                intent.putExtra("city", city);
                startActivity(intent);

            }
        });

        demo_Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NotificationManager mNotificationManager = (NotificationManager) getActivity().getSystemService(Context.NOTIFICATION_SERVICE);
                String channel_ID = "com.example.holstonjx1.finalprogram";
                CharSequence title = "Nearby Concerts";
                String description = "There are concerts nearby.";
                int importance = NotificationManager.IMPORTANCE_HIGH;

                if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
                {
                    NotificationChannel channel = new NotificationChannel(channel_ID, title, importance);
                    channel.setDescription(description);
                    channel.enableLights(true);
                    channel.setLightColor(R.style.AppTheme);
                    channel.enableVibration(true);
                    channel.setVibrationPattern(new long[]{1000});
                    mNotificationManager.createNotificationChannel(channel);
                }

                NotificationCompat.Builder builder = new NotificationCompat.Builder(getContext(), channel_ID)
                    .setSmallIcon(R.drawable.top_logo)
                    .setContentTitle(title)
                    .setContentText(description)
                    .setChannelId(channel_ID);

                PendingIntent contentIntent = PendingIntent.getActivity(getContext(), 0, new Intent(getContext(), MainActivity.class), PendingIntent.FLAG_UPDATE_CURRENT);
                /* Intent resultIntent = new Intent(getActivity(), HomeFragment.class);
                TaskStackBuilder stackBuilder = TaskStackBuilder.create(getActivity());
                stackBuilder.addParentStack(getActivity());
                stackBuilder.addNextIntent(resultIntent);
                PendingIntent pendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT); */
                builder.setContentIntent(contentIntent);
                builder.setAutoCancel(true);
                mNotificationManager.notify(1, builder.build());

                SharedPreferences sharedPref = getActivity().getSharedPreferences("common_file", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putBoolean("notification", true);
                editor.commit();
            }
        });
        return v;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void loadComplete() {
        data = singleton.getInstance(getContext()).data;

        lm = (LocationManager) getContext().getSystemService(Context.LOCATION_SERVICE);

        temp = true;
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            temp = false;
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 75);
        }

        if(temp) {
            Location location = lm.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            double longitude = location.getLongitude();
            double latitude = location.getLatitude();
            deviceLocation = new LatLng(latitude, longitude);

            taskParams params = new taskParams(deviceLocation, data, getActivity(), nearbyListView);
            if(!singleton.getInstance().nearbyLoaded) {
                nearbyThread thread = new nearbyThread();
                thread.execute(params); // nearby data sorted by this call, and listview adapter set in onPostExecute
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 75: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    temp = true;
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.

                    System.out.println("no permissions");
                }
            }
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
