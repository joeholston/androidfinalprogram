package com.example.holstonjx1.finalprogram;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.support.v4.app.ActivityCompat;
import android.widget.ListView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.maps.model.LatLng;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.StringTokenizer;

/**
 * Created by SIMEKQW1 on 11/12/2017.
 */

public class singleton {
    private ArrayList<ShowInfo> nearbyData;// = new ArrayList<>();
    private static Context mContext;
    private RequestQueue queue;
    private static singleton ourInstance;
    private boolean isLoaded;
    private LocationManager lm;
    public ArrayList<ShowInfo> data;
    private singletonDelegate delegate;
    public boolean nearbyLoaded = false;
    public int curPage = -1;

    public static singleton getInstance(Context temp) {
        if(ourInstance == null)
        {
            ourInstance = new singleton(temp);
        }

        return ourInstance;
    }
    public static singleton getInstance() {
        return ourInstance;
    }

    public Context getAContext() {
        return mContext;
    }

    private singleton(Context temp) {
        mContext = temp;
        data = new ArrayList<ShowInfo>();
    }

    private RequestQueue getQueue()
    {
        if(queue == null)
        {
            queue = Volley.newRequestQueue(mContext);
        }

        return queue;
    }
    public ArrayList<ShowInfo> getNearby() {
        return new ArrayList<> (nearbyData);
    }
    public void giveNearby(ArrayList<ShowInfo> in) {
        nearbyData = new ArrayList<> (in);
    }


    public void loadData(singletonDelegate d)
    {
        String url = "https://en.wikipedia.org/w/api.php?action=query&titles=Emotional%20Roadshow%20World%20Tour&prop=revisions&rvprop=content&format=json";

        delegate = d;
        if(isLoaded)
        {
            delegate.loadComplete();
            return;
        }

        JsonObjectRequest jRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                JSONObject obj2;
                try {
                    //Sets up the real JSON reading work.
                    obj2 = response.getJSONObject("query");
                    JSONObject obj3 = obj2.getJSONObject("pages");
                    JSONObject obj4 = obj3.getJSONObject("50492050");

                    //Because Wikipedia did not provide real JSON data, I had to create a new system to break apart the data I did have. This is that system.
                    String tempStore = obj4.get("revisions").toString();
                    //Break apart at every instance of n\\|.
                    String delimiter = "n\\| ";
                    int i = 0;
                    boolean gotCity = false;
                    boolean gotVenue = false;
                    //RowCity is used to show a city in multiple rows if there's a rowspan tag.
                    int rowCity = 0;
                    int rowVenue = 0;
                    //This is where I put the city that I repeat for multiple rows.
                    String repeatCity = "NULL - UNINITIALIZED";
                    String repeatVenue = "NULL - UNINITIALIZED";
                    //Split the string into its pieces.
                    for (String retval: tempStore.split(delimiter)) {
                        ShowInfo temp1 = new ShowInfo();
                        //System.out.println(i+ ": " + retval);

                        //This code verifies I have a date. If I do, then I add it to the dates column. If not, continue checking.
                        if (i >= 17 && (retval.charAt(0) != '[') && retval.charAt(0) != '{' && retval.length() > 9 && !retval.startsWith("rowspan") &&
                                (retval.startsWith("January") || retval.startsWith("February") || retval.startsWith("March") || retval.startsWith("April") || retval.startsWith("May")) || retval.startsWith("June") || retval.startsWith("July") || retval.startsWith("August") || retval.startsWith("September") || retval.startsWith("October") || retval.startsWith("November") || retval.startsWith("December")) {
                            retval = retval.substring(0, retval.indexOf("201") + 4);
                            //if(retval.lastIndexOf('{')!= -1) {
                            //    retval = retval.substring(0,retval.indexOf('{'));
                            //}
                            temp1.date = retval;
                            //Initialize the other pieces, and reset the gotCity variable, which I use to try to keep track of whether or not I have the current city.
                            temp1.venue = "[Data not parceable]";
                            temp1.city = "[Data not parceable]";
                            gotCity = false;
                            data.add(temp1);
                        }
                        // If I already have a city value that's supposed to repeat, I use that value again instead of trying to get a new one.
                        if(rowCity > 0) {
                            data.get(data.size()-1).city = repeatCity;
                            rowCity--;
                            gotCity = true;
                            gotVenue = false;
                        }
                        //Get a city if I don't already have one and this one doesn't have a rowspan value.
                        else if(i>=17 && (retval.substring(0,2)).equals("[[") && !gotCity) {
                            retval = retval.substring(2,retval.length()-3);

                            if(retval.lastIndexOf('|') != -1) {
                                retval = retval.substring(0,retval.indexOf('|'));
                            }
                            data.get(data.size()-1).city = retval;
                            //System.out.println(retval);
                            gotCity = true;
                            gotVenue = false;
                        }else if(retval.length() >= 17 && retval.substring(0,8).equals("rowspan=") && retval.charAt(15)=='[' && !gotCity) {
                            if(retval.charAt(11)=='\\') {
                                //Single digit rowspan value
                                rowCity = Character.digit(retval.charAt(10), 10);

                            } else {
                                //Double digit rowspan value
                                rowCity = Character.digit(retval.charAt(10), 10)*10;
                                rowCity += Character.digit(retval.charAt(11), 10);
                            }
                            //Getting the weird brackets off of the data.
                            retval = retval.substring(retval.indexOf('[') + 2, retval.indexOf(']'));
                            if(retval.lastIndexOf('|') != -1) {
                                retval = retval.substring(0,retval.indexOf('|'));
                            }
                            repeatCity = retval;
                            data.get(data.size()-1).city = repeatCity;
                            //rowCity--;
                            gotCity = true;
                            gotVenue = false;
                            //System.out.println("City = " + retval);

                        }

                        //Now for the venues:

                        //Repeat roughly the same process for the venue.
                        if(gotCity && !gotVenue && i >17 && (retval.substring(0,2)).equals("[[")) {
                            retval = retval.substring(2,retval.length()-3);

                            if(retval.indexOf(']') != -1) {
                                retval = retval.substring(0,retval.indexOf(']'));
                            }
                            if(retval.indexOf('|') != -1) {
                                retval = retval.substring(retval.indexOf('|')+1, retval.length());
                            }
                            data.get(data.size()-1).venue = retval;
                            //System.out.println(retval);
                            gotVenue = true;
                            rowVenue = 0;
                        }
                        i++;
                    }
                    //Some printed checks:
                    //for(int i = 0; i < splitArr.length; i++) {
                    //    System.out.println(splitArr[i]);
                    //}
                    //for(int k = 0; k < data.size(); k++) {
                        //System.out.println(data.get(k).date);
                        //System.out.println(data.get(k).city);
                        //System.out.println(data.get(k).venue);
                    //}

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        getQueue().add(jRequest);
        isLoaded = true;
    }
}
